{
    description = "tcp-hello-world";

    inputs = {
        nixpkgs.url = github:NixOS/nixpkgs;
        flake-utils.url = "github:numtide/flake-utils";
    };

    outputs = { self, flake-utils, nixpkgs }:
        flake-utils.lib.eachDefaultSystem (system:
            let
                pkgs = import nixpkgs { inherit system; };
            in {
                devShell = pkgs.stdenv.mkDerivation {
                    name = "tcp-hello-world";
                    buildInputs = [
                        pkgs.cargo
                        pkgs.rustc
                        pkgs.rustfmt
                        pkgs.rust-analyzer
                    ];
                };
            }
        );
}
