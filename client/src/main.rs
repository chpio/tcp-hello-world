use ::anyhow::Result;
use ::std::{io::Read, net::TcpStream};

fn main() -> Result<()> {
    let mut stream = TcpStream::connect("127.0.0.1:34254")?;

    let mut buf = Vec::new();
    stream.read_to_end(&mut buf)?;

    println!("from server: {}", String::from_utf8(buf)?);

    Ok(())
}
