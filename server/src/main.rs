use ::anyhow::Result;
use ::std::{
    io::Write,
    net::{TcpListener, TcpStream},
    time::SystemTime,
};

fn handle_client(mut stream: TcpStream) -> Result<()> {
    println!("connection established from {}", stream.peer_addr()?);

    let timestamp = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH)?;

    let response = format!("hello, the current timestamp is {}", timestamp.as_secs());

    stream.write_all(response.as_bytes())?;

    println!("disconnecting");

    Ok(())
}

fn main() -> Result<()> {
    let listener = TcpListener::bind("127.0.0.1:34254")?;

    for stream in listener.incoming() {
        handle_client(stream?)?;
    }

    Ok(())
}
